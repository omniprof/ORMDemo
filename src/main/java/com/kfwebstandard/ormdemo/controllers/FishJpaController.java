/*
 * Sample code for students written by Ken Fogel.
 * Creative Commons Licence
 * Freely use portions of this code in your solutions.
 * Provide attribution by referencing the git repository
 * that this code came from. 
 */
package com.kfwebstandard.ormdemo.controllers;

import com.kfwebstandard.ormdemo.controllers.exceptions.NonexistentEntityException;
import com.kfwebstandard.ormdemo.controllers.exceptions.RollbackFailureException;
import com.kfwebstandard.ormdemo.entities.Fish;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author omni_
 */
@Named
@RequestScoped
public class FishJpaController implements Serializable {

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "fishPU")
    private EntityManager em;    

    public void create(Fish fish) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            em.persist(fish);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public void edit(Fish fish) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            fish = em.merge(fish);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = fish.getId();
                if (findFish(id) == null) {
                    throw new NonexistentEntityException("The fish with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Fish fish;
            try {
                fish = em.getReference(Fish.class, id);
                fish.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The fish with id " + id + " no longer exists.", enfe);
            }
            em.remove(fish);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Fish> findFishEntities() {
        return findFishEntities(true, -1, -1);
    }
    
//    public List<Fish> getAll() {
//        return findFishEntities(true, -1, -1);
//    }

   
    public List<Fish> findFishEntities(int maxResults, int firstResult) {
        return findFishEntities(false, maxResults, firstResult);
    }

    private List<Fish> findFishEntities(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Fish.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
     }

    public Fish findFish(Integer id) {
            return em.find(Fish.class, id);
    }

    public int getFishCount() {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Fish> rt = cq.from(Fish.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();

        
    }
    
}
